Name:            clucene
Version:         2.3.3.4
Release:         38
Summary:         CLucene is a C++ port of Lucene
License:         LGPL-2.1-or-later or Apache-2.0
URL:             http://www.sourceforge.net/projects/clucene
Source0:         clucene-core-2.3.3.4-e8e3d20.tar.xz
BuildRequires:   boost-devel cmake gawk gcc-c++ zlib-devel
Patch0000:       0000-clucene-core-2.3.3.4-pkgconfig.patch
Patch0001:       0001-clucene-core-2.3.3.4-install_contribs_lib.patch
Patch0002:       0002-Fix-missing-include-time.h.patch
Patch0003:       clucene-core-2.3.3.4-CLuceneConfig.patch
Patch0004:       clucene-port-to-newer-cmake.patch

%description
CLucene is a C++ port of Lucene: the high-performance, full-featured
text search engine written in Java. CLucene is faster than lucene
as it is written in C++.

%package core
Summary:        Core clucene module
Provides:       clucene = %{version}-%{release}

%description core
CLucene is a C++ port of Lucene. It is a high-performance, full-featured
text search engine written in C++. CLucene is faster than lucene
as it is written in C++.

%package core-devel
Summary:        Development files for clucene library
Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-contribs-lib = %{version}-%{release}

%description core-devel
CLucene is a C++ port of Lucene. It is a high-performance, full-featured text
search engine written in C++. CLucene is faster than lucene as it is written
in C++.
This package holds the development files for clucene.

%package contribs-lib
Summary:        Language specific text analyzers for %{name}
Requires:       %{name}-core = %{version}-%{release}

%description contribs-lib
%{summary}.

%prep
%autosetup -n %{name}-core-%{version} -p1

rm -rfv src/ext/{boost/,zlib/}


%build
%{cmake} \
  -DBUILD_CONTRIBS_LIB:BOOL=ON -DLIB_DESTINATION:PATH=%{_libdir} \
  -DLUCENE_SYS_INCLUDES:PATH=%{_libdir} \
%cmake_build

%install
%cmake_install

%check
export PKG_CONFIG_PATH=%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion libclucene-core)" = "%{version}"
# FIXME: make tests non-fatal for ppc and s390 (big endian 32 bit archs) until we have a proper fix
export CTEST_OUTPUT_ON_FAILURE=1
# needing the 'touch' here seems an odd workaroudn for missing dependency, race condition or cache requirement
touch src/test/CMakeLists.txt && \
make -C %{__cmake_builddir} cl_test && \
time make -C %{__cmake_builddir} test ARGS="--timeout 300 --output-on-failure" ||:

%files core
%license APACHE.license COPYING LGPL.license
%doc AUTHORS ChangeLog README
%{_libdir}/libclucene-core.so.1
%{_libdir}/libclucene-core.so.%{version}
%{_libdir}/libclucene-shared.so.1
%{_libdir}/libclucene-shared.so.%{version}

%files contribs-lib
%{_libdir}/libclucene-contribs-lib.so.1
%{_libdir}/libclucene-contribs-lib.so.%{version}

%files core-devel
%dir %{_libdir}/CLucene
%{_includedir}/CLucene/
%{_includedir}/CLucene.h
%{_libdir}/libclucene*.so
%{_libdir}/CLucene/clucene-config.h
%{_libdir}/CLucene/CLuceneConfig.cmake
%{_libdir}/pkgconfig/libclucene-core.pc

%changelog
* Sun Mar 02 2025 Funda Wang <fundawang@yeah.net> - 2.3.3.4-38
- try build with cmake 4.0

* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 2.3.3.4-37
- adopt to new cmake macro

* Thu Jul 13 2023 liyanan <thistleslyn@163.com> - 2.3.3.4-36
- fix build error

* Sun Dec 1 2019 wangzhishun <wangzhishun1@huawei.com> - 2.3.3.4-35
- Package init
